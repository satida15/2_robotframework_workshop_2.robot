***Settings***
Library  BuiltIn
Library  SeleniumLibrary


***Variables***

${url}=  https://ptt-devpool-robotframework.herokuapp.com/
${inputUsername}=  admin
${inputPassword}=  1234

&{dict1}=  fristname=David  lastname=Beckham  email=David.Beckham@gmail.com
&{dict2}=  fristname=johny  lastname=Depp  email=johny.Depp@gmail.com
&{dict3}=  fristname=Robert  lastname=Downey  email=Robert.Downey@gmail.com
&{dict4}=  fristname=Tom  lastname=Hanks  email=Tom.Hanks@gmail.com

@{list}=  dict1  dict2  dict3  dict4

    

***Test Cases***
1. เปิด browser
    Open browser  about.blank  browser=Chrome
    Maximize browser Window
    Set Selenium Speed  0.5



2. ไปที่เว็บไซต์ 
    Go To  ${url}


3. กรอกข้อมูล username password
    Input Text  id=txt_username  ${inputUsername}
    Input Text  id=txt_password  ${inputPassword}

4. กด submit
    Click button  id=btn_login

5. กดเพิ่มข้อมูล
    Click button  id=btn_new
    
6. กรอกข้อมูล fristname lastname e-mail
    FOR  ${index}  IN  @{list}
        LOOP  &{${index}}
        Click Button  id=btn_new
    END
    Click Button  id=btn_new_close

7. Screenshot
    Capture Page    Screenshot  filename=${CURDIR}/screenshot.png


*** Keywords ***
LOOP
    [Arguments]    &{paralist}
    FOR  ${key}  ${valuelist}  IN  &{paralist}
            Run Keyword If  '${key}' == 'fristname'   Input Text  id=txt_new_firstname  ${valuelist}
            Run Keyword If  '${key}' == 'lastname'   Input Text  id=txt_new_lastname  ${valuelist}
            Run Keyword If  '${key}' == 'email'   Input Text  id=txt_new_email  ${valuelist}
    END
    Click Button  id=btn_new_save



 
    






